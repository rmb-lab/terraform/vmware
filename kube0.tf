data "vsphere_datacenter" "lab" {
  name = "Lab"
}

data "vsphere_datastore" "datastore" {
  name          = "datastore"
  datacenter_id = "${data.vsphere_datacenter.lab.id}"
}

data "vsphere_datastore" "datastore1" {
  name          = "datastore1"
  datacenter_id = "${data.vsphere_datacenter.lab.id}"
}

data "vsphere_compute_cluster" "management" {
  name          = "Management"
  datacenter_id = "${data.vsphere_datacenter.lab.id}"
}

data "vsphere_network" "vm-lan" {
  name          = "vm-lan"
  datacenter_id = "${data.vsphere_datacenter.lab.id}"
}

data "vsphere_virtual_machine" "centos-template" {
  name          = "centos-7-2018-04-01T18.57.41"
  datacenter_id = "${data.vsphere_datacenter.lab.id}"
}

resource "vsphere_virtual_machine" "kube0-master" {
  name             = "kube0-master.vmw.rmb938.me"
  resource_pool_id = "${data.vsphere_compute_cluster.management.resource_pool_id}"
  datastore_id     = "${data.vsphere_datastore.datastore.id}"

  num_cpus = 4
  memory   = 4096
  guest_id = "${data.vsphere_virtual_machine.centos-template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.centos-template.scsi_type}"

  network_interface {
    network_id   = "${data.vsphere_network.vm-lan.id}"
    adapter_type = "${data.vsphere_virtual_machine.centos-template.network_interface_types[0]}"
  }

  disk {
    label            = "Hard disk 1"
    size             = 128
    thin_provisioned = true
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.centos-template.id}"
  }

  enable_disk_uuid = true
}

resource "vsphere_virtual_machine" "kube0-worker" {
  count            = 3
  name             = "kube0-worker${count.index+1}.vmw.rmb938.me"
  resource_pool_id = "${data.vsphere_compute_cluster.management.resource_pool_id}"
  datastore_id     = "${data.vsphere_datastore.datastore1.id}"

  num_cpus = 4
  memory   = 8192
  guest_id = "${data.vsphere_virtual_machine.centos-template.guest_id}"

  scsi_type = "${data.vsphere_virtual_machine.centos-template.scsi_type}"

  network_interface {
    network_id   = "${data.vsphere_network.vm-lan.id}"
    adapter_type = "${data.vsphere_virtual_machine.centos-template.network_interface_types[0]}"
  }

  disk {
    label            = "Hard disk 1"
    size             = 128
    thin_provisioned = true
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.centos-template.id}"
  }

  enable_disk_uuid = true
}
