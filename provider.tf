terraform {
  backend "gcs" {
    bucket = "rmb-lab-terraform"
    prefix = "vmware"
  }
}

provider "vsphere" {
  user           = "terraform@vsphere.local"
  password       = "${var.vsphere_password}"
  vsphere_server = "vcenter.rmb938.me"

  allow_unverified_ssl = true
}
